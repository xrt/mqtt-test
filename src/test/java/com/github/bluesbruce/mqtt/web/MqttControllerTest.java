package com.github.bluesbruce.mqtt.web;

import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class MqttControllerTest extends TestWebBase {

  private MockMvc mvc;
  @Resource
  private MqttController controller;

  @Before
  public void setControllerBuilder() {
    mvc = MockMvcBuilders.standaloneSetup(controller).build();
  }

  /**
   * 测试Controller
   *
   * @throws Exception 抛出异常
   */
  @Test
  public void ControllerListTest() throws Exception {
    String message = "mqttTest测试";
    MvcResult result = mvc.perform(
        //使用get模式
        MockMvcRequestBuilders.post("/mqtt")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .param("msg", message)
            .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andReturn();
    String body = result.getResponse().getContentAsString();
    LOGGER.info("------response-body-------");
    LOGGER.info(body);
    LOGGER.info("-------------");
  }

}